from random import randint
import numpy as np
from math import ceil, pi, floor

# a = randint(ord('A'), ord('Z'))
# print('the number is %d and the character is %s'%(a,chr(a)))

def getCapLetter():
    return randint(ord('A'), ord('Z'))

def getSmallLetter():
    return randint(ord('a'), ord('z'))
def getNumber():
    return randint(0,9)
    

def generatePass(passlen, times):
    temp = chr(getCapLetter())
    for j in range(times):
        for i in range(passlen-1):
            r = randint(0,2)
            if r == 0:
                temp += (chr(getSmallLetter())) 
            elif r == 1:
                temp += (chr(getCapLetter()))
            else:
                temp += str(getNumber())
        temp += '\n'
    return temp

print(generatePass(20, 5))

