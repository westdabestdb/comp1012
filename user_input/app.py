from time import ctime
# number = input('Enter a number: ') #normal input
# print(type(number))
# print(type(int(number))) #turn type string into int
# print(type(eval(number))) #turn type string into 'eval' --> automatically detects the type
# print('hello' * 3) # prints 'hellohellohello'
# print('hello' / 3) # gives an error

def getInt():
    number = input('Enter a number: ').strip()
    if number != '':
        try:
            number = eval(number, {}, {}) #automatically detects the type
        except:
            print('Invalid input!')
        else:
            if type(number) is int: #checks the type
                print('%d is an integer type'%number)
            elif type(number) is float:
                print('%.f is a float type'%number)
            else:
                print(number, 'is not an integer type')
    else:
        print('Missing an input')


def getInt2(prompt):
    while True:
        number = input(prompt).strip()
        if number != '':
            try:
                number = eval(number, {}, {}) # automatically detects the type
            except:
                print('Invalid input!')
            else:
                if type(number) is int: # checks the type
                    print('%d is an integer type'%number)
                    break
                else:
                    print(number, 'is not an integer type')
        else:
            # missing an input
            break
    return number        


def displayList(headingMessage, theList):
    print('\n%s'%headingMessage)
    for element in theList:
        print(element)
    print('There are %d elements are in the list'%len(theList))

def displayTerminationMessage():
    print('''
Programmed by Gorkem Erol.
Date: %s
End of processing.''' % ctime())


def main():
    numbers = []
    number = getInt2('Enter an integer: ')
    while number != '':
        numbers.append(number)
        number = getInt2('Enter an integer: ')
    displayList('The elements in the list of numbers are:', numbers)
    displayTerminationMessage()


def getValidInt(prompt):
    minX = 3
    ifContinute = True
    while ifContinute:
        number = input(prompt).strip()
        if number != '': #check if input is empty
            try:
                number = eval(number,{},{})
            except:
                print('Invalid input')
            else:
                if type(number) is int: #check the input is an integer
                    if number%2 == 1: #check the input is odd number
                        if number >= minX: #check the input is larger than minimum value
                            drawX(number)
                            ifContinute = getBoolean('Do you want to draw another X ? (Y/N): ')
                        else:
                            print('Number cannot be smaller than %d'%minX)
                    else:
                        print('You need to enter a odd number')
                else:
                    print('%.2f is not an integer'%number)
        else:
            break
                

def drawX(sizeX):
    for r in range(sizeX):
        for c in range(sizeX):
            if r==c or r+c==sizeX-1:
                print('*',end='')
            else:
                print(' ', end='')
        print()


def getBoolean(prompt):
    while True:
        answer = input(prompt).strip()
        answer = answer[0]
        if answer == 'Y' or answer == 'y':
            result = True
            break
        elif answer == 'N' or answer == 'n':
            result = False
            break
        else:
            print(answer, 'is not a valid response!')
    return result
            
    
def calculator():
    smallest = -111
    largest = 999
    count = 0
    total = 0
    isContinue = True
    firstTime = True
    while isContinue:
        try: 
            num = input('Enter a number: ').strip()
        except EOFError:
            isContinue = False
        else: 
            try:
                num = eval(num,{},{})
            except:
                print('Invalid input')
            else:
                if type(num) is int or type(num) is float:
                    if firstTime:
                        count += 1
                        total += num
                        smallest = num
                        largest = num
                        firstTime = False
                    else:
                        if num > largest:
                            largest = num
                            count += 1
                            total += num
                        if num < smallest:
                            smallest = num
                            count += 1
                            total += num

    print('''
    Smallest is: %.2f
    Largest is: %.2f
    Average is: %.2f
    Total is: %.2f  
    '''%(smallest,largest,total/count,total))


                        
calculator()


# getInt()

# getInt2('Enter a number: ')

# main()


# getValidInt('Enter a X number to create a star: (X >= 3)')

