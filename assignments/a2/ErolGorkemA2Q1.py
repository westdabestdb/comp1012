# ErolGorkemA2Q1.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignement: 2 Question 1
# Author: Gorkem Erol
# Version: 2018/06/10
#
# Purpose: The purpose of the program.
#
from time import ctime

def displayTerminationMessage():
    print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())

def getSymbols():
    symbols = []
    number = 0

    while len(symbols) <= 0 and number == 0:
        num = int(input('Enter the number of symbols: '))
        if(num < 0):
            print('%i must be > 0'%num)
        else:
            number = num
    
    while number > 0:
        symbol = input('Enter a 1 character symbol: ')
        if(len(symbol) > 0):
            symbols.append(symbol)
        number -= 1

    return symbols


def generateSequences(symbols):
    arr = []
    length = len(symbols)
    for n in range(length**length):
        arr.append("".join(symbols[n // length**(length-d-1) % length] for d in range(length)))
    
    return arr


def countWinners(sequences):
    winnerCount = 0
    for i in sequences:
        if(i == len(i) * i[0]):
            winnerCount += 1
    print('There are %i winning sequences out of %i total sequences.'%(winnerCount, len(sequences)))


def displaySequences(sequences):
    for i in sequences:
        print(i)


def main():
    seqs = generateSequences(getSymbols())
    displaySequences(seqs)
    countWinners(seqs)
    displayTerminationMessage()


main()
