# ErolGorkemA2Q3.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignement: 2 Question 3
# Author: Gorkem Erol
# Version: 2018/06/10
#
# Purpose: The purpose of the program.
#

import numpy as np
from math import exp, pi
from decimal import Decimal
from time import ctime


def displayTerminationMessage():
    print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())


def computeXcoordinates(xSemiAxis, intervals):
    return np.linspace(0,xSemiAxis,intervals)

def computeAreas(xSemiAxis, ySemiAxis, zSemiAxis, xCoordinates):
    ratioYX = ySemiAxis / xSemiAxis
    ratioZX = zSemiAxis / xSemiAxis
        
    x = np.array(xCoordinates)
    semiAxis1 = np.array(ySemiAxis - ratioYX * x)
    semiAxis2 = np.array(zSemiAxis - ratioZX * x)

    area = np.array(np.pi * semiAxis1 * semiAxis2)
    return area


def computeVolume(xSemiAxis, intervals, areas):
    vl = []
    vol = 0
    for i in range((intervals)-1):
        q = (areas[i] + areas[i+1])
        vl.append(q)
   
    for i in vl:
        vol += i
        
    vol *= 2*xSemiAxis/intervals
    
    return vol
    

def main():
    toGo = True
    while toGo:

        interval = int(input('Enter the number of intervals (0 to quit):').strip())

        if interval < 0:
            interval = input('Enter the number of intervals (0 to quit):').strip()
        elif interval == 0:
            toGo = False
            break
        else:
            xLen = float(input('Enter the length of the X semi-axis in cm (> 0):').strip())
            if xLen <= 0:
                xLen = float(input('Enter the length of the X semi-axis in cm (> 0):').strip())
            
            yLen = float(input('Enter the length of the Y semi-axis in cm (> 0):').strip())
            if yLen <= 0:
                yLen = float(input('Enter the length of the X semi-axis in cm (> 0):').strip())
            
            zLen = float(input('Enter the length of the Z semi-axis in cm (> 0):').strip())
            if zLen <= 0:
                zLen = float(input('Enter the length of the Z semi-axis in cm (> 0):').strip())
        
            xCoor = computeXcoordinates(xLen, interval)
            areas = computeAreas(xLen, yLen, zLen, xCoor)
            apprx = computeVolume(xLen, interval, areas)
            actual = 4/3 * pi * xLen * yLen * zLen
            print('The actual volume of the ellipsoid is %.14e cm^3' % Decimal(actual))
            print('The approximate volume of the ellipsoid is %.14e cm^3' % Decimal(apprx))
            print('The error in the approximate volume is %.6e cm^3' % Decimal(actual-apprx))
    displayTerminationMessage()
        

main()