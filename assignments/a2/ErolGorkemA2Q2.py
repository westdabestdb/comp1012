# ErolGorkemA2Q2.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignement: 2 Question 2
# Author: Gorkem Erol
# Version: 2018/06/10
#
# Purpose: The purpose of the program.
#
from time import ctime

fractionCount = 0

def displayTerminationMessage():
    print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())


def getPositiveInt(prompt):
    i = int(input(prompt))
    while i <= 0:
        i = int(input(prompt))
    
    return i


def getFraction(one=1.0):
    global fractionCount
    tstr = ''
    if fractionCount == 0:
        tstr = 'first'
    elif fractionCount == 1:
         tstr = 'second'
        
    print('Enter %s fraction'%tstr)
    n = eval(input('Enter the numerator as an integer:'))
    d = eval(input('Enter the denominator as an integer:'))
    while d <= 0:
        print('The denominator cannot be zero!')
        d = int(input('Enter the denominator as an integer:'))
    
    c = n/d
    fractionCount += 1
    return c


def displayFraction(fraction, places):
   
    fr = '{:.{pr}f}'.format(fraction, pr=places)
    print(fr)  


def main():
    i = getPositiveInt('Enter the number of decimal places (>0): ')
    while True:
        
        f1 = getFraction(i)
        if f1 == 0:
            break
        else:
            displayFraction(f1,50)
            f2 = getFraction(i)
            displayFraction(f2,50)

            print('The product of the fractions is: ')
            displayFraction(f1*f2,50)


            print('-' * 50)
            global fractionCount
            fractionCount = 0
    
    displayTerminationMessage()


main()