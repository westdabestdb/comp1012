# ErolGorkemA2Q4.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignement: 2 Question 4
# Author: Gorkem Erol
# Version: 2018/06/10
#
# Purpose: The purpose of the program.
#

import numpy as np
from math import cos, sin
from time import ctime
def displayTerminationMessage():
    print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())


def computeCoordinates(intervals, maxAngle, scale):
    xCoor = []
    yCoor = []
    interval = maxAngle / intervals
    angles = [i*interval for i in range(intervals+1)]
    radii = [scale * a for a in angles]

    rC = 0
    while rC < len(radii):
        xc = radii[rC] * cos(angles[rC])
        xCoor.append(xc)
        yc = radii[rC] * sin(angles[rC])
        yCoor.append(yc)
        rC += 1
            
    return angles, radii, xCoor, yCoor


def displayCoordinates(angles, radii, xCoords, yCoords):
    print('Polar Angle\t\tCoordinates Radius\t\tCartesian X coordinat\t\tCoordinates Y coordinate')
    i = 0
    while i < len(angles):
        a = format(angles[i], '.4f')
        r = format(radii[i], '.4f')
        x = format(xCoords[i], '.6f')
        y = format(yCoords[i], '.6f')
        print(a, '\t\t\t', r, '\t\t\t', x, '\t\t\t', y)
        i += 1


def main():
    interval = int(input('Enter the number of intervals:').strip())
    if interval <= 0:
        interval = int(input('Enter the number of intervals:').strip())
    else:
        angle = float(input('Enter the maximum angle in radians:').strip())
        if angle <= 0:
            print('The maximum angle, %s, must be greater than zero.'%(angle))
            angle = float(input('Enter the maximum angle in radians:').strip())
        else:
            scale = float(input('Enter the scale factor for the spiral:').strip())
            if scale <= 0:
                print('The scale factor, %s, must be greater than zero.'%(scale))
                scale = float(input('Enter the scale factor for the spiral:').strip())
            else:
                cc = computeCoordinates(interval, angle, scale)
                displayCoordinates(cc[0],cc[1],cc[2],cc[3])
                displayTerminationMessage()

main()
            
