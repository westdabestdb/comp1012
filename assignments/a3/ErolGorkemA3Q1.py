# ErolGorkemA3Q1.py
#
# Course: COMP 1012
# Instructor: Amirhossei Hosseinmemar
# Assignement: 3 Question 1
# Author: Gorkem Erol
# Version: 2018/07/20
#
# Purpose: Calculating the area under curve using lists.
#
# var1 - the use / meaning of each variable in the program
# my result is very close but not equal to yours

from time import ctime
import numpy as np
import timeit
from decimal import Decimal
print ('\n---------------------------------------------\n')

def displayTerminationMessage():
    print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())

def getPositiveNumber(prompt, EOF):
    while True:
        number = input(prompt).strip()
        if number != '':
            try:
                number = eval(number, {}, {})
            except:
                print('%r is not valid!'%number)
            else:
                if type(number) is int or type(number) is float:
                    if number > 0:
                        break
                    elif number == 0:
                        number = EOF
                        break
                    else:
                        print('%d is less than zero!'%number)
                        
                else:
                    print('%r is not a number!'%number)
        else:
            print('Missing input!')
    
    return number
        

def computeXcoordinates(h, intervals):
    xCoor = [0 + x*(2*h - 0)/intervals for x in range(intervals+1) ]
    return xCoor


def parabola(h, k, xCoordinates):
    a = float(-k / h**2)
    yCoor = [a * ((x - h) ** 2) + k for x in xCoordinates]
    return yCoor


def computeArea(h, k, intervals):
    area = 0
    xCoor = computeXcoordinates(h, intervals)
    yCoor = parabola(h, k, xCoor)
    area = np.trapz(yCoor, xCoor)
    actualArea = 4 * h * k / 3.

    print('Approximate area under the parabola is %.14e' % area)
    print('Actual area is %.14e' % actualArea)
    print('The error in the approximate area is %.6e' % abs(actualArea - area))
        

EOF = -1
while True:
    h = getPositiveNumber('Enter the value of h in cm (0 to quit): ',EOF)
    if h == EOF:
        break
    k = getPositiveNumber('Enter the value of k in cm (0 to quit): ',EOF)
    if k == EOF:
        break
    intervals = getPositiveNumber('Enter the number of intervals (0 to quit): ',EOF)
    if intervals == EOF:
        break
    t = timeit.Timer('computeArea(h,k,intervals)',
    'from __main__ import computeArea, h, k , intervals')
    computeTime = t.timeit(1)
    print("Compute time using lists is %.3f seconds." % computeTime)

displayTerminationMessage()