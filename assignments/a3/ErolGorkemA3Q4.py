# ErolGorkemA3Q4.py
#
# Course: COMP 1012
# Instructor: Amirhossei Hosseinmemar
# Assignement: 3 Question 4
# Author: Gorkem Erol
# Version: 2018/07/20
#
# Purpose: Answers.
#
# var1 - the use / meaning of each variable in the program

print('(a) 4.900 seconds.')
print('(b) 5.932 seconds.')
print('(c) 0.278 seconds.')
print('(d) second program ran slowest.')
print('(e) second program ran fastest.')
print('(f) lists.')