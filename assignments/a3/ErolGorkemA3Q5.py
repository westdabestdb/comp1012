# ErolGorkemA3Q5.py
#
# Course: COMP 1012
# Instructor: Amirhossei Hosseinmemar
# Assignement: 3 Question 5
# Author: Gorkem Erol
# Version: yyyy/mm/dd
#
# Purpose: Sorting and grouping words from file
#
# var1 - the use / meaning of each variable in the program

from sys import path
from os import chdir
chdir(path[0])


def getWords(filename):
    infile = open(filename, 'r')
    data = infile.read()
    infile.close()
    return data.split()


def sortLettersInWord(word):
    return ''.join(sorted(word))


def buildSortedWordsList(words):
    l = [sortLettersInWord(word) for word in words]
    uniqWords = []
    for w in l:
        if w not in uniqWords:
            uniqWords.append(w)
    return uniqWords


def buildAnagramLists(words, sortedWords):
    finalList = list()
    for i in range(len(sortedWords)):
        nl = list()
        for w in words:
            if sortedWords[i] == sortLettersInWord(w):
                nl.append(w)
        finalList.append(nl)
    return finalList
        

def displayAnagrams(uniqueSortedWords, anagramWordLists):
    print('Letters\t\tWords')
    for i in range(len(uniqueSortedWords)):
        print('%-16s%s'%(uniqueSortedWords[i], ''.join(str(w + ' ') for w in anagramWordLists[i])))


def main():
    file = input('Enter the name of the file containing the words: ').strip()
    words = getWords(file)
    sorted_words = buildSortedWordsList(words)
    anagrams = buildAnagramLists(words,sorted_words)
    displayAnagrams(sorted_words,anagrams)


main()