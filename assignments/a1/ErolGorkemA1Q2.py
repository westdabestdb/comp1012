# ErolGorkemA1Q2.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignment: 1 Question 2
# Author: Gorkem Erol
# Version: 2018/06/02
#
# Purpose: The purpose of the program.
#
# var1 - the use / meaning of each variable in the program

from time import ctime
from math import exp
print('\n---------------------------------------------\n')
print('Calculate (1+x)e^x given the number of decimal places and the value of x')

d = int(input('Enter the number of decimal places: '))
x = int(input('Enter the value of x: '))

val = (1+x) * exp(x)
print("Python's values of (3)e^2 is:")
print("%.14e"%val)

print("The approximate value of (3)e^2 is:")
print("%.*f"%(d,val))

print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())
