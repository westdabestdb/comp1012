# ErolGorkemA1Q1.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignment: 1 Question 1
# Author: Gorkem Erol
# Version: 2018/06/02
#
# Purpose: The purpose of the program.
#
# var1 - the use / meaning of each variable in the program

from time import ctime
from math import pi, cos, sin, sqrt, atan, degrees
print('\n---------------------------------------------\n')

a = float(input('Enter the real part of the complex number: '))
b = float(input('Enter the imaginary part of the complex number: '))
r = sqrt(a**2+b**2)
complexNum = a+b*1j

print("The complex number is", complexNum)
print()
k = 0
theta = atan(b/a)
while k < 3:
    rq = r**0.3333333333333334
    rx = rq * ( cos((2*pi * k + theta)/3) + 1j * sin((2*pi * k + theta)/3) )
    print("root%i = "%(k+1), rx)
    print("root%i%s"%(k+1,'^3 = '),rx**3)
    print()
    k += 1


print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())
