# ErolGorkemA1Q2.py
#
# Course: COMP 1012
# Instructor: Amirhossein Hosseinmemar
# Assignment: 1 Question 2
# Author: Gorkem Erol
# Version: 2018/06/02
#
# Purpose: The purpose of the program.
#
# var1 - the use / meaning of each variable in the program

from time import ctime
from math import sinh
print('\n---------------------------------------------\n')
print('Calculate (1+x)e^x given the number of decimal places and the value of x')

x = float(input('Enter the value of x: '))
tolerance = float(input('Enter the tolerance: '))

val = sinh(x)
print("Python's value of sinh(%.2f) is %e"%(x,val))
print("The approximate value of sinh(%.2f) is %.14e"%(x,val))

print("""
Programmed by Gorkem Erol
Date: %s
End of processing.""" % ctime())
