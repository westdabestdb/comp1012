def f(x):
    if x == 0:
        result = 0
    elif x%3 == 0:
        result = 3
    elif x%2 == 0:
        result = 2
    else:
        result = 1
    return result

values = list(range(-6,7,1))
print(' x\tf(x)')
for val in values:
    print('%2.i\t%i'%(val,f(val)))

#  x      f(x)
# -6       3
# -5       1
# -4       2
# -3       3
# -2       2
# -1       1
#  0       0
#  1       1
#  2       2
#  3       3
#  4       2
#  5       1
#  6       3