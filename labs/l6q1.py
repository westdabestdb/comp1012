def getPosInt(prompt):
    run = True
    while run:
        num = input(prompt).strip()
        if num != '':
            try:
                num = eval(num,{},{})
            except:
                print('Invalid input!')
            else:
                if type(num) is int:
                    if num < 0:
                        print('%i is not a positive integer!'%num)
                    else:
                        run = False
                else:
                    print('%s is not an integer!'%num)
        else:
            print('Missing input!')
    return num
                

def drawTriangle(size):
    while size > 0:
        print('*' * size, end='\n')
        size -= 1


def main():
    while True:
        i = getPosInt('Enter the size of the triangle (0 to end):')
        if i == 0:
            break
        else:
            print('Draw a triangle whose size is %i'%i)
            drawTriangle(i)

main()