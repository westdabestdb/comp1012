import numpy as np
import matplotlib.pyplot as plt
def plotSubPlots(x, y2, y3, y4, y5, y6, y7):
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(x, y2, 'r-', x, y3, 'b-', x, y4, 'g-')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(['1-1/x','1/2-1/(2x^2)','1/3-1/(3x^2)'],\
                loc='center',bbox_to_anchor=(0.5, 0.875), ncol=3)
    plt.title('Diminshing Function')
    
    plt.subplot(2, 1, 2)
    plt.plot(x, y5, 'm-', x, y6, 'c-', x, y7, 'y-')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(['(x^3-1)/3','(x^2-1)/2','x-1'], loc='best')
    plt.title('Increasing Function')
    plt.tight_layout()
    plt.savefig('lab9q1.png') 
    plt.show() 
    
x = np.arange(1.0,5.0,0.1)
y2 = 1.0-1.0/x
y3 = 0.5 - 1.0/(2*x**2)
y4 = 1.0/3.0 - 1.0/(3.0*x**3)
y5 = (x**3-1.0)/3.0
y6 = (x**2-1.0)/2.0
y7 = x-1.0

plotSubPlots(x,y2,y3,y4,y5,y6,y7)