from numpy.random import uniform
from numpy import pi
from time import ctime


def MonteAreaEllipse(a, b, points):
    xCoordinates = uniform(-a, a, points)
    yCoordinates = uniform(-b, b, points)
    inCircle = (xCoordinates/a)*2+(yCoordinates/b)*2 <= 1
    probability = sum(inCircle) / points
    area = probability * 4. * a * b
    return area, probability


def displayTerminationMessage():
    print("""\nProgrammed by gorkem erol)
Date: %s
End of processing.""" % ctime())


def main():
    while True:
        a = float(input('Enter the length of the semi-major axis in cm (> 0): '))
        if a <= 0:
            print('The semi-major axis must be greater than zero.')
            break
        else:
            b = float(
                input('Enter the length of the semi-minor axis in cm (> 0): '))
        if b <= 0:
            print('The semi-minor axis must be greater than zero.')
            break
        else:
            actarea = pi*a*b
            points = 10000000
            area, probability = MonteAreaEllipse(a, b, points)
            error = abs(area-actarea)
            print("""
The probability of a point being in the ellipse is %.6f
The approximate area of the ellipse is %.12e cm^2
     The actual area of the ellipse is %.12e cm^2
     The error in the approximation is %.6e cm^2
     """ % (probability, area, actarea, error))
            break

    displayTerminationMessage()


main()
