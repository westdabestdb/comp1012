def validateBoundedInt(value, bound):
    if type(value) is not int:
        message = '%s is not an integer!'%value
    else:
        if value < bound*-1:
            message = '%i is too small!'%value
        elif value > bound:
            message = '%i is too large!'%value
        else:
            message = '%i is a valid integer!'%value
    return message

boundVal = 100
values = [-101, -100, -1, 0, 1, 101, 100, True, 1.5, 'hello']

for val in values:
    print('%s'%validateBoundedInt(val, boundVal))

# -101 is too small!
# -100 is a valid integer!
# -1 is a valid integer!
# 0 is a valid integer!
# 1 is a valid integer!
# 101 is too large!
# 100 is a valid integer!
# True is not an integer!
# 1.5 is not an integer!
# hello is not an integer!