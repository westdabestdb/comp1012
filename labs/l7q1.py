import numpy as np

def getNumber(prompt):
    isinvalid = True
    while isinvalid:
        num = input(prompt).strip()
        if num != '':
            try:
                num = eval(num,{},{})
            except:
                print('Invalid input!')
                isinvalid = True
            else:
                if type(num) is int or type(num) is float:
                    return num
                else:
                    print('%s is not a numberxxx!'%num)
        else:
            print('Missing input!')
            isinvalid = True



def yOnEllipse(a, b, xValues):
    x = np.array(xValues)
    yValues = b * np.sqrt(1 - (x**2 / a**2))
    return yValues


def verifyPoints(a, b, xValues, yValues):
    vX = np.array(xValues)
    vY = np.array(yValues)
    vP = np.array((vX**2 / a**2) + (vY**2 / b**2) == 1)
    return vP


def main():
    a = getNumber('Enter the length of the major axis:')
    b = getNumber('Enter the length of the minor axis:')
    nop = getNumber('Enter the number of points along the major axis:')
    xVal = np.linspace(0,a,nop)
    yValues = yOnEllipse(a, b, xVal)
    v = verifyPoints(a, b, xVal, yValues)

    st = 'X\t\t\tY\t\t\tOn Ellipse'
    print(st)
    print('-' * len(st)*3)
    i = 0
    while i < nop:
        print('%f\t\t%f\t\t%s'%(xVal[i], yValues[i], v[i]))
        i += 1


main()
