from time import ctime

aa  = {'a', 'b'}
# print(type(aa))

a = 1.23
b = 3.22
c = -10.2
z = {a,b,c,}
# print(z)

s5 = set()
s5.add(1.27)
# print(s5)

s1 = {1, 2, 3}
s2 = {2, 'a', 'b', 'c', 'd'}
s3 = {1, 3, 5, 7}
s4 = {2, 4, 6, 8}

ul = s1.union(s2) # removes duplicates
il = ul.intersection(s3) # gets commons
# print(ul)
# print(il)

s3.isdisjoint(s4) #true
ul.isdisjoint(s2) #false


wordlist = ['hello',1,2,3]
ws = set(wordlist)
# print(ws)

temps = {
    'Winnipeg': -30.7,
    'Victoria': 3.1,
    'Halifax': -6.5
}
# print(temps)

t = {
    'weatherData': {
        'halifax': {
            'temp': -38.0,
            'population': '480k'
        },
        'winnipeg': {
            'temp': -30.7,
            'population': '920k'
        }
    },
    'year': 2018,
}
# print(t['weatherData']['winnipeg'])

# for key in t.keys():
#     if key == 'weatherData':
#         print('Cities:')
#         for c in t[key].keys():
#             print('\t%s'%c)
#             for kx, val in t[key][c].items():
#                 print('\t\t%s: %s'%(kx, val))


# def displayFrequencies(freq):
#     print('\n%10s %10s' % ('Number', 'Frequency'))
#     for num in freq:
#         print('%10d %10d' % (num, freq[num]))

# dataSet = {
#     'countries': {
#         'Canada': {
#             'Capital': 'Ottowa',
#             'Population': '32M'
#         },
#         'Turkey': {
#             'Capital': 'Ottowa',
#             'Population': '32M'
#         },
#         'Germany': {
#             'Capital': 'Ottowa',
#             'Population': '32M'
#         },
#         'Brazil': {
#             'Capital': 'Ottowa',
#             'Population': '32M'
#         },
#         'USA': {
#             'Capital': 'Ottowa',
#             'Population': '32M'
#         }
#     }
# }

countries = ['Canada', 'Turkey', 'Germany', 'Brazil', 'USA']
capitals = ['Ottowa', 'Ankara', 'Berlin', 'Brasiliza', 'Washington' ]
population = ['36.29 million', '79.51 million', '82.67 million', '207.7 million', '325.7 million' ]

def printL(countries, capitals, population):
    print('Countries:')
    for i in range(len(countries)):
        print('\tCountry: %s'%countries[i])
        print('\t\tCapital: %s'%capitals[i])
        print('\t\tPopulation: %s'%population[i])
        

printL(countries,capitals, population)

        