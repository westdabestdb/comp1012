import time
from math import pi, sin

a = 20
b = -2.5

# return calculated result
def toFahrenheit(celsius):
    return 9./5 * celsius + 32

# return calculated result
def evalQuadratic(a,b,c,x):
    print('a = %g, b = %g, c = %g, x = %g' % (a,b,c,x))
    return a*x**2 + b*x + c

# does not have return
def displayTerminationMessage():
    print('''
    Programmed by westdabestdb
    Date: %s
    End of processing    
    ''' %time.ctime())

# return calculated result
def double(val):
    return 2*val

# define a local 'a' variable
def func1(x):
    a = 1
    return a * b * x

# change global 'a' variable
def func2(x):
    global a
    a = 10
    return a * b * x

# return calculated result as tuple
def f(x):
    return x, x**2, x**3, x**4

# pre-defined parameter values
def myFunc(pos1, pos2, key1='FirstKey', key2='SecondKey'):
    print('pos1 = %s, pos2 = %s, key1 = %s, key2 = %s' %(pos1, pos2, key1, key2))


def areaRectangle(length, width):
    return length * width


def areaRightTriangle(height, width):
    return height*width/2

# use function in a function
def area(fx, v1, v2):
    return fx(v1,v2)
    
# if else example
def f2(x):
    if 0 <= x <= pi/2:
        result = sin(x)
    else:
        result = 0.0
    return result

# no else example
def f3(x):
    result = 0.0
    if 0 <= x <= pi/2:
        result = sin(x)
    return result

# elif example
def f4(x):
    if x < -10:
        result = 0
    elif x < 0:
        result = -x
    elif x <= 10:
        result = x
    else:
        result = 0
    return result

# two options merged in one
def f5(x):
    if x < -10 or 10 < x:
        result = 0
    elif x < 0:
        result = -x
    else:
        result = x
    return result

# nested if else
def testNum(x):
    if type(x) is int or type(x) is float:
        if x == 0:
            result = 'zero'
        elif x < 0:
            result = 'negative'
        else:
            result = 'positive'
    else:
        result = 'invalid type'
    return result

###### UNCOMMENT TO RUN CODES ######

# print(toFahrenheit(20))

# print(evalQuadratic(3, -1, 10, 2))

# displayTerminationMessage()

# myResult = evalQuadratic(3, -1, 10, 2)
# print(myResult)

# aa, bb, cc, xx = -2, 3, -1, 3
# fx = evalQuadratic(aa*3, bb -1, (cc+10)/bb, double(xx))
# print(fx)

# celsius = [c * 5 -20 for c in range(9)]
# print(celsius)
# fahrenheit = [toFahrenheit(c) for c in celsius]
# print(fahrenheit)

# print(func1(2))
# print(func2(1))
# print(a)

# x,x2,x3,x4 = f(2)
# print(x,x2,x3,x4)

# myFunc(10, -2.5, 'hi', 'bye')
# myFunc(10, -2.5)

# print(area(areaRectangle, 2, 3))
# print(area(lambda length, width: length*width, 2,3)) #lambda function

# print(area(areaRightTriangle, 4, 12))
# print(area(lambda height, width: height*width/2, 4, 12)) #lambda function

# sin = f2(1)
# print(sin)

# sin = f3(1)
# print(sin)

# print(f4(8))

# print(f5(8))

# a = 20
# b = -2.5
# c = 100
# d = -100
# print(c if a < b else d)

# k1 = 4
# mt = lambda k2: k1 if k2 < k1 else k2
# result = mt(4)
# print(result)

# myAge = lambda age: age if age > 0 else 0
# print(myAge(19))
# print(myAge(-19))

# print(testNum(12))
# print(testNum(-12))
# print(testNum(0))
# print(testNum('fff'))