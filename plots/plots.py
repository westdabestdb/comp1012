import matplotlib.pyplot as plt
import numpy as np

# def plotCurve(xCoor, yCoor):
#     plt.figure()
#     plt.plot(xCoor, yCoor)
#     plt.savefig('fig1.png')
#     plt.show()


# def plotCurve(xCoor, yCoor):
#     plt.figure()
#     plt.plot(xCoor, yCoor)
#     plt.xlabel('x')
#     plt.ylabel('y')
#     plt.axis([-6., 6., -1., 25.])
#     plt.legend(['x^2'])
#     plt.title('f(x)=x^2')
#     plt.savefig('fig2.png')
#     plt.show()

def plotCurve(xCoor, ys2, ys3):
    plt.figure()
    plt.plot(xCoor, ys2, 'r-')
    plt.plot(xCoor, ys3, 'b-')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(['x^2', 'x^3'], loc='upper right')
    plt.title('f(x)=x^2 vs f(x)=x^3')
    plt.savefig('fig3.png')
    plt.show()


def subplots(x, y2, y3):
    plt.figure()
    plt.subplot(2,1,1)
    plt.plot(x, y2, 'r-', x, y3, 'b-')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(['x^2', 'x^3'], loc='lower right')
    plt.title('f(x)=x^2 vs f(x)=x^3')

    plt.subplot(2,1,2)
    plt.plot(x, y2, 'm--', x, y3, 'c--')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(['x^2', 'x^3'], loc='best')
    plt.title('f(x)=x^2 vs f(x)=x^3')

    plt.tight_layout()
    plt.savefig('subplots.png')
    plt.show()
    

def main():
    # xCoor = np.arange(-5., 5.1, .1)
    # yCoor = xCoor ** 2
    # plotCurve(xCoor, yCoor)

    xCoor = np.arange(-5., 5.1, .1)
    ys2 = xCoor ** 2
    ys3 = xCoor ** 3
    # plotCurve(xCoor, ys2, ys3)
    subplots(xCoor, ys2, ys3)

main()